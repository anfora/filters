from setuptools import setup

config = {
	'name': 'anforaFilters',
	'version': '0.1',
	'description': 'Instagram-like image filters',
	'license': 'LICENSE.txt',
	'url': 'https://github.com/anforaProject/filters',
	'author': 'Yábir Garcia',
	'author_email': 'yabirg@protonmail.com',
	'install_requires': ['pillow'],
	'packages': ['instagram_filters', 'instagram_filters.filters', 'instagram_filters.decorations'],
	'data_files' : [
		('instagram_filters/decorations/frames', ['instagram_filters/decorations/frames/Kelvin.jpg', 'instagram_filters/decorations/frames/Nashville.jpg'])
	],
	'zip_safe': False
}

setup(**config)
